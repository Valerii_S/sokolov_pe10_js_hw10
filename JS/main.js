const showPass = document.getElementsByClassName('icon-password');

for (let i = 0; i < showPass.length; i++) {
    showPass[i].addEventListener('click', showPassword);
}

function showPassword() {
    const show = document.getElementById(this.dataset.target);
    if (show.type === 'password') {
        show.setAttribute('type', 'text');
        this.setAttribute('class','fas fa-eye-slash icon-password');
    } else {
        show.setAttribute('type', 'password');
        this.setAttribute('class','fas fa-eye icon-password');
    }
}
const submit = document.getElementsByClassName('btn');

for (let i = 0; i < submit.length; i++) {
    submit[i].addEventListener('click', formCheck);
}

const p = document.createElement('p');
p.setAttribute('class', 'pStyle');
const cont = document.getElementById('container');
cont.appendChild(p);

function formCheck(){
    const form1 = document.getElementById('showPass').value;
    const form2 = document.getElementById('showPassToo').value;
        if(form1 === form2){
            p.innerHTML = '';
            alert('You are welcome!');
        }
        else {
            p.innerHTML = 'Enter the same password please';
         }
}